import cocotb
from cocotb.triggers import ClockCycles, FallingEdge


class Timing:
    TIME_0 = 6
    TIME_1 = 12
    TIME_TOTAL = 17
    TIME_DELAY = 200


async def decode_dshot_frame(clk, s) -> int:
    result = 0
    for _ in range(0, 16):
        for i in range(0, 3):
            await FallingEdge(clk)
            if s.o.is_eq("true"):
                break
        if s.o.is_eq("false"):
            raise Exception("Bit did not arrive in 3 cycles")

        stop_time = None
        for i in range(0, Timing.TIME_TOTAL-1):
            await FallingEdge(clk)
            if s.o.is_eq("false"):
                stop_time = i
                break
        if stop_time is None:
            raise Exception("Found no end of the byte sequence")
        for i in range(0, Timing.TIME_TOTAL - stop_time-1):
            await FallingEdge(clk)
            s.o.assert_eq("false")

        result = (result << 1) | (stop_time > Timing.TIME_0)

    return result

# Ported from cleanflight
# https://github.com/cleanflight/cleanflight/blob/bd523ced7cc3a8c000068cca08da96eff43716a1/src/main/drivers/pwm_output.c#L400C1-L417C2
def encode_dshot(value: int, telemetry: bool):
    packet = (value << 1) | telemetry;

    # compute checksum
    csum = 0;
    csum_data = packet
    for _ in range(0, 3):
        csum ^=  csum_data
        csum_data >>= 4
    csum &= 0xf;

    packet = (packet << 4) | csum;

    return packet;
