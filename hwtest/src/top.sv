module top(
    input clk_25mhz,
    input sbus_rx,
    output[7:0] led,
    output servo_out,
    input [4:0] buttons,
    output dshot_out
);
    reg rst = 1;
    always @(posedge clk_25mhz) begin
        rst <= 0;
    end

    wire dummy;

    main main
        ( .clk_25_i(clk_25mhz)
        , .rst_i(rst)
        , .sbus_rx_i(sbus_rx)
        , .leds_o(led)
        , .servo_o(servo_out)
        , .output__(dummy)
        , .buttons_i(buttons)
        , .dshot_o(dshot_out)
        );
endmodule
