use protocols::uart::uart_rx;
use protocols::uart::UartConfig;
use protocols::UartOut;

#[no_mangle]
fn launder_sbus_frame(in: int<200>) -> RawSbusFrame __builtin__

// Because of Spade array packing, we need to index servos in 'reverse order', so
// let's wrap things in a nice struct to avoid missing that detail
struct SbusServos {
    inner: [int<11>; 16]
}

impl SbusServos {
    fn get_signed(self, idx: int<4>) -> int<11> {
        trunc(self.inner[trunc(idx)] - 1023)
    }

    fn get_unsigned(self, idx: int<4>) -> int<12> {
        zext(self.inner[trunc(idx)])
    }
}

struct RawSbusFrame {
    footer: int<8>,
    padding: int<4>,
    frame_lost: bool,
    failsafe: bool,
    chan18: bool,
    chan17: bool,
    servos: SbusServos,
    header: int<8>,
}

impl RawSbusFrame {
    entity track_valid(self, clk: clock, rst: bool) -> Option<SbusFrame> {
        reg(clk) last_valid reset (rst: None()) =
            if self.header == 0x0f && self.footer == 0x00 {
                let RawSbusFrame$(
                    servos,
                    chan17,
                    chan18,
                    frame_lost,
                    failsafe,
                    header: _,
                    footer: _,
                    padding: _
                ) = self;

                Some(SbusFrame$(
                    servos,
                    chan17,
                    chan18,
                    frame_lost,
                    failsafe
                ))
            }
            else {
                last_valid
            };
        last_valid
    }
}

struct SbusFrame {
    servos: SbusServos,
    chan17: bool,
    chan18: bool,
    frame_lost: bool,
    failsafe: bool,
}

struct SbusConfig {
    khz100: int<27>,
}

// NOTE: Uart config should have a baud rate of 100_000
entity sbus_decoder(clk: clock, rst: bool, rx: bool, cfg: SbusConfig) -> Option<SbusFrame> {
    let uart_out = inst uart_rx$(
        clk,
        rst,
        rx: !rx,
        config: UartConfig$(bit_time: cfg.khz100, stop_bits: 2, parity: true)
    );

    reg(clk) received_bytes reset(rst: 0) = match uart_out {
        // TODO: Handle parity failure
        Some(val) => (val.strip_parity() `concat` trunc(received_bytes >> 8)),
        None => received_bytes
    };

    let raw_frame = launder_sbus_frame(received_bytes);
    let sbus_frame = raw_frame
        .inst track_valid(clk, rst);

    sbus_frame
}

struct TestHarnessOut {
    frame: Option<SbusFrame>,
    servo_out: Option<int<12>>
}

entity sbus_decoder_test_harness(
    clk: clock,
    rst: bool,
    rx: bool,
    cfg: SbusConfig,
    servo_idx: int<4>
) -> TestHarnessOut {
    let out = inst sbus_decoder$(clk, rst, rx, cfg);

    let servo = match out {
        Some(frame) => Some(frame.servos.get_unsigned(servo_idx)),
        None => None()
    };
    TestHarnessOut(out, servo)
}
