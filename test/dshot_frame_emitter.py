# top = dshot::frame_emitter

import os
import sys

from cocotb.clock import Clock
from spade import SpadeExt

import cocotb
from cocotb.triggers import ClockCycles, FallingEdge

sys.path.append(os.environ["SWIM_ROOT"] + "/testutil")
from dshot import decode_dshot_frame, Timing


@cocotb.test()
async def frame_reception_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    s.i.rst = "true"
    await FallingEdge(clk);
    s.i.rst = "false"
    s.i.timings = f"""DshotTimings$(
        ns652: {Timing.TIME_0},
        ns1250: {Timing.TIME_1},
        ns1670: {Timing.TIME_TOTAL},
        ms2: {Timing.TIME_DELAY}
    )"""

    s.i.word = "Some(0b1100_1010_1111_0000u)"

    out = await decode_dshot_frame(clk, s)
    print(f"{out:b} {0b1100_1010_1111_0000}")
    assert out == 0b1100_1010_1111_0000

