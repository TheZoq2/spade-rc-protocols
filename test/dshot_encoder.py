# top = dshot::dshot_encoder

import os
import sys

from cocotb.clock import Clock
from spade import SpadeExt

import cocotb
from cocotb.triggers import Timer, FallingEdge

sys.path.append(os.environ["SWIM_ROOT"] + "/testutil")
from dshot import encode_dshot


@cocotb.test()
async def disarm_sends_0(dut):
    s = SpadeExt(dut)

    s.i.command = "MotorCommand::Disarm()"

    expected_val = encode_dshot(0, False)

    await Timer(1, units = "ns")

    s.o.assert_eq(f"{expected_val}")


@cocotb.test()
async def arm_0_sends_0(dut):
    s = SpadeExt(dut)

    s.i.command = "MotorCommand::Run(0)"

    expected_val = encode_dshot(48, False)

    await Timer(1, units = "ns")

    s.o.assert_eq(f"{expected_val}")




@cocotb.test()
async def arm_1000_sends_1048(dut):
    s = SpadeExt(dut)

    s.i.command = "MotorCommand::Run(1000)"

    expected_val = encode_dshot(1048, False)

    await Timer(1, units = "ns")

    s.o.assert_eq(f"{expected_val}u")


@cocotb.test()
async def arm_1999_sends_2047(dut):
    s = SpadeExt(dut)

    s.i.command = "MotorCommand::Run(1999)"

    expected_val = encode_dshot(2047, False)

    await Timer(1, units = "ns")

    s.o.assert_eq(f"{expected_val}u")


@cocotb.test()
async def arm_2001_caps_to_2000(dut):
    s = SpadeExt(dut)

    s.i.command = "MotorCommand::Run(2001u)"

    expected_val = encode_dshot(2047, False)

    await Timer(1, units = "ns")

    s.o.assert_eq(f"{expected_val}u")

@cocotb.test()
async def arm_2047_caps_to_2000(dut):
    s = SpadeExt(dut)

    s.i.command = "MotorCommand::Run(2047u)"

    expected_val = encode_dshot(2047, False)

    await Timer(1, units = "ns")

    s.o.assert_eq(f"{expected_val}u")

@cocotb.test()
async def sweep_params(dut):
    s = SpadeExt(dut)

    for i in range(0, 2000, 4):
        s.i.command = f"MotorCommand::Run({i})"

        expected_val = encode_dshot(i + 48, False)

        await Timer(1, units = "ns")

        s.o.assert_eq(f"{expected_val}u")
