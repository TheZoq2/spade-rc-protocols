#top = servo::servo_driver

from cocotb.clock import Clock
from spade import SpadeExt

import cocotb
from cocotb.triggers import ClockCycles, FallingEdge

TIME_1MS=1000

async def test_count(dut, command, duration):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    s.i.command_in = f"{command}"
    s.i.rst = "true"
    await FallingEdge(clk);
    s.i.rst = "false"
    s.i.cfg = f"ServoConfig$(ms1: {TIME_1MS}, ms1_5: {int(TIME_1MS * 1.5)}, ms20: {int(TIME_1MS * 20)})"

    for _ in range(0, int(TIME_1MS*duration)-1):
        await FallingEdge(clk);
        s.o.assert_eq("true")

    # We can tolerate being off by a tiny bit
    await FallingEdge(clk);
    await FallingEdge(clk);

    for _ in range(0, int(TIME_1MS * (20 - duration)-1)):
        await FallingEdge(clk);
        s.o.assert_eq("false")


@cocotb.test()
async def test_center(dut):
    await test_count(dut, 0, 1.5)

@cocotb.test()
async def test_min(dut):
    await test_count(dut, -1024, 1)

@cocotb.test()
async def test_max(dut):
    await test_count(dut, 1023, 2)
