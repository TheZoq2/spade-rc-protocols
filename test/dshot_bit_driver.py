# top = dshot::bit_driver_harness

from cocotb.clock import Clock
from spade import SpadeExt

import cocotb
from cocotb.triggers import ClockCycles, FallingEdge

TIME_0 = 6
TIME_1 = 12
TIME_TOTAL = 17
TIME_DELAY = 200


@cocotb.test()
async def ready_before_command(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    s.i.rst = "true"
    await FallingEdge(clk);
    s.i.rst = "false"
    s.i.timings = f"""DshotTimings$(
        ns652: {TIME_0},
        ns1250: {TIME_1},
        ns1670: {TIME_TOTAL},
        ms2: {TIME_DELAY}
    )"""

    s.i.bit = "None()"

    await FallingEdge(clk);
    s.o.val.assert_eq("false")
    s.o.ready.assert_eq("true")
    await FallingEdge(clk);
    s.o.val.assert_eq("false")
    s.o.ready.assert_eq("true")


@cocotb.test()
async def sending_bit0_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    s.i.rst = "true"
    await FallingEdge(clk);
    s.i.rst = "false"
    s.i.timings = f"""DshotTimings$(
        ns652: {TIME_0},
        ns1250: {TIME_1},
        ns1670: {TIME_TOTAL},
        ms2: {TIME_DELAY}
    )"""

    s.i.bit = "None()"

    await FallingEdge(clk);
    s.o.val.assert_eq("false")
    s.o.ready.assert_eq("true")

    s.i.bit = "Some(false)"
    await FallingEdge(clk);
    s.i.bit = "None()"

    for _ in range(0, TIME_0):
        s.o.val.assert_eq("true")
        s.o.ready.assert_eq("false")
        await FallingEdge(clk);

    for _ in range(0, TIME_TOTAL - TIME_0):
        s.o.val.assert_eq("false")
        s.o.ready.assert_eq("false")
        await FallingEdge(clk);

    # Ready for another bit and low
    s.o.val.assert_eq("false")
    s.o.ready.assert_eq("true")


@cocotb.test()
async def sending_bit1_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    s.i.rst = "true"
    await FallingEdge(clk);
    s.i.rst = "false"
    s.i.timings = f"""DshotTimings$(
        ns652: {TIME_0},
        ns1250: {TIME_1},
        ns1670: {TIME_TOTAL},
        ms2: {TIME_DELAY}
    )"""

    s.i.bit = "None()"

    await FallingEdge(clk);
    s.o.val.assert_eq("false")
    s.o.ready.assert_eq("true")

    s.i.bit = "Some(true)"
    await FallingEdge(clk);
    s.i.bit = "None()"

    for _ in range(0, TIME_1):
        s.o.val.assert_eq("true")
        s.o.ready.assert_eq("false")
        await FallingEdge(clk);

    for _ in range(0, TIME_TOTAL - TIME_1):
        s.o.val.assert_eq("false")
        s.o.ready.assert_eq("false")
        await FallingEdge(clk);

    # Ready for another bit and low
    s.o.val.assert_eq("false")
    s.o.ready.assert_eq("true")
