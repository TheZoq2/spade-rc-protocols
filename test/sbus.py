#top = sbus::sbus_decoder_test_harness

from cocotb.clock import Clock
from spade import SpadeExt

import cocotb
from cocotb.triggers import ClockCycles, FallingEdge

BIT_TIME=10

async def wait_whole_frame(clk):
    bit_count = 1 + 8 + 2 + 1
    [await FallingEdge(clk) for i in range(0, bit_count * BIT_TIME * 24)]

async def send_byte_lsb_first(s, clk, byte: int):
    s.i.rx = "true" # start bit
    await ClockCycles(clk, BIT_TIME, rising = False)
    for b in reversed(f"{byte:8b}"):
        val = "false" if b == "1" else "true"
        s.i.rx = val
        await ClockCycles(clk, BIT_TIME, rising = False)

    s.i.rx = "false" if f"{byte:b}".count("1") % 2 == 1 else "true" # parity
    await ClockCycles(clk, BIT_TIME, rising = False)
    s.i.rx = "false" # stop bit
    await ClockCycles(clk, BIT_TIME, rising = False)
    s.i.rx = "false" # stop bit
    await ClockCycles(clk, BIT_TIME, rising = False)


@cocotb.test()
async def without_rx_no_valid_frame_is_received(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    s.i.rx = "false"
    s.i.rst = "true"
    await FallingEdge(clk);
    s.i.rst = "false"
    s.i.cfg = f"SbusConfig$(khz100: {BIT_TIME})"

    await wait_whole_frame(clk)
    await wait_whole_frame(clk)
    s.o.frame.assert_eq("None()")

@cocotb.test()
async def valid_frame_with_all_zeros_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    s.i.rx = "false"
    s.i.rst = "true"
    await FallingEdge(clk);
    s.i.rst = "false"
    s.i.cfg = f"SbusConfig$(khz100: {BIT_TIME})"

    await send_byte_lsb_first(s, clk, 0x0f)

    for _ in range(0, 23):
        await send_byte_lsb_first(s, clk, 0)
    await send_byte_lsb_first(s, clk, 0)

    await ClockCycles(clk, BIT_TIME, rising=False)
    s.o.frame.assert_eq("""Some(SbusFrame $(
            servos: SbusServos([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
            chan17: false,
            chan18: false,
            frame_lost: false,
            failsafe: false
        ))""")

@cocotb.test()
async def first_servo_channel_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    s.i.rx = "false"
    s.i.rst = "true"
    await FallingEdge(clk);
    s.i.rst = "false"
    s.i.cfg = f"SbusConfig$(khz100: {BIT_TIME})"

    await send_byte_lsb_first(s, clk, 0x0f)

    # First servo channel
    await send_byte_lsb_first(s, clk, 0b1100_1010)
    await send_byte_lsb_first(s, clk, 0b0000_0010)

    for _ in range(0, 22):
        await send_byte_lsb_first(s, clk, 0)
    await send_byte_lsb_first(s, clk, 0)

    s.i.servo_idx = "0"

    await ClockCycles(clk, BIT_TIME, rising=False)
    s.o.frame.assert_eq("""Some(SbusFrame $(
            servos: SbusServos([0b010_1100_1010u, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
            chan17: false,
            chan18: false,
            frame_lost: false,
            failsafe: false
        ))""")

    s.o.servo_out.assert_eq("Some(0b010_1100_1010)")
    s.i.servo_idx = "1"

    await ClockCycles(clk, BIT_TIME, rising=False)
    s.o.servo_out.assert_eq("Some(0)")


